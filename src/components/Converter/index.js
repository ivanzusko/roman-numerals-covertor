import React, { useState } from 'react'
import RomanNumerals from '../../roman-numerals'

import './styles.css'

function Converter () {
  const [number, setNumber] = useState('')
  const [romanNumber, setRomanNumber] = useState('')
  const [converted, setConverted] = useState(false)

  const handleConvertRomans = e => {
    e.preventDefault()

    try {
      const regular = RomanNumerals.fromRoman(romanNumber)
      setNumber(regular)
      setConverted(true)
    } catch (error) {
      alert(error)
    }
  }

  const handleConvertRegular = e => {
    e.preventDefault()

    try {
      const roman = RomanNumerals.toRoman(number)

      setRomanNumber(roman)
      setConverted(true)
    } catch (error) {
      alert(error)
    }
  }

  const handleNumberInput = e => {
    if (converted) {
      setNumber('')
      setRomanNumber('')
      setConverted(false)
    }
    const { value } = e.target

    setNumber(value)
  }

  const handleRomansInput = e => {
    if (converted) {
      setNumber('')
      setRomanNumber('')
      setConverted(false)
    }
    const { value } = e.target

    setRomanNumber(value)
  }

  return (
    <div className='Romans'>
      <div className='Converter'>
        <form className='Form' name='toRomans' onSubmit={handleConvertRegular}>
          <label className='Form-title' htmlFor='regular'>To romans</label>
          <div className='Input'>
            <div className='Input-wrapper'>
              <input className='Input-input' id='regular' type='number' onChange={handleNumberInput} value={number} />
            </div>
            <button className='Button' type='submit'>Become a Roman!</button>
          </div>
        </form>

        <form className='Form' onSubmit={handleConvertRomans}>
          <label className='Form-title' htmlFor='romans'>From romans</label>
          <div className='Input'>
            <div className='Input-wrapper'>
              <input className='Input-input' id='romans' type='text' onChange={handleRomansInput} value={romanNumber} />
            </div>
            <button className='Button Button--Barbarian' type='submit'>Become a barbarian :)</button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Converter
