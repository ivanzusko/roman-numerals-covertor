import React from 'react'
import Converter from '../Converter'

import './styles.css'

export default function App () {
  return (
    <div className='App'>
      <h1 className='App-title'>Vae victis!</h1>
      <Converter />
    </div>
  )
}
