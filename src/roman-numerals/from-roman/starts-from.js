export default function startsFrom (number, query) {
  return String(number).startsWith(query)
}
