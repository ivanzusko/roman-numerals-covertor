import { fromRoman } from './index'

test('fromRoman', () => {
  expect(fromRoman('I')).toEqual(1)
  expect(fromRoman('IX')).toEqual(9)
  expect(fromRoman('XX')).toEqual(20)
  expect(fromRoman('LXXXVII')).toEqual(87)
  expect(fromRoman('lXXxVII')).toEqual(87)
  expect(fromRoman('XC')).toEqual(90)
  expect(fromRoman('XCVIII')).toEqual(98)
  expect(fromRoman('DCCC')).toEqual(800)
  expect(fromRoman('CM')).toEqual(900)
  expect(fromRoman('MMMMCMXCIX')).toEqual(4999)

  expect(() => fromRoman('VX')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('VL')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('VC')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('VD')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('VM')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('LM')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('LC')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('LD')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('XD')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('VX')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('IL')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('XM')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('XLM')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('vx')).toThrowError('Please, provide a valid Roman numeral')
  expect(() => fromRoman('funFunFunction')).toThrowError('Please, provide a valid Roman numeral')
})
