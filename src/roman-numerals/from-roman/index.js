import startsFrom from './starts-from'
import { invalidRomanNumeral } from '../utils/errors'

/**
 * @name fromRoman
 * @description this method converts Roman numerals to regular number
 * @param {String} roman string with Roman numeral
 *
 * @returns {Number} number converted from the Roman numeral
 */
export function fromRoman (roman) {
  const romanMap = {
    I: {
      value: 1,
      range: [1, 10]
    },
    V: {
      value: 5,
      range: [5, 10]
    },
    X: {
      value: 10,
      range: [10, 100]
    },
    L: {
      value: 50,
      range: [50, 100]
    },
    C: {
      value: 100,
      range: [100, 1000]
    },
    D: { value: 500, range: [500, 1000] },
    M: { value: 1000, range: [1000] }
  }
  let result

  /**
   * 1. they compute from left to right
   * 2. if current smaller then next - current become negative(gets '-')
   *
   * Example:
   * XIX  10-1+10
   * XX   10+10
   * XL   -10+50
   * XLI  -10+50+1
   * XLIV -10+50-1+5
   */

  // we need to convert Roman to an array
  result = roman.toUpperCase().split('')

  // we need assign regular values to each roman
  // and provide max which is value of roman numeral which is allowed to be the next
  // if further the max value will be less then value of next roman numeral -> we will throw
  result = result.map(roman => {
    if (!romanMap[roman]) invalidRomanNumeral()

    return {
      value: romanMap[roman].value,
      max: romanMap[roman].range[1]
    }
  })

  // we need to assign +/-
  result = result.map((item, index) => {
    const next = result[index + 1]

    // if current less then next => current is negative
    if (next && item.value < next.value) {
      // throw if:
      // - max value is smaller then value of next roman
      // - value starts from 5
      if (startsFrom(item.value, 5) || item.max < next.value) {
        invalidRomanNumeral()
      }

      return item.value * -1
    }

    return item.value
  })

  // we need compute
  result = result.reduce((regular, number) => {
    return regular + number
  }, 0)

  return result
}
