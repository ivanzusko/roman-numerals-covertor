import { toRoman } from './to-roman'
import { fromRoman } from './from-roman'

const RomanNumerals = {
  fromRoman,
  toRoman
}

export default RomanNumerals
