import getSeparately from '../get-separately'

/**
 *
 * @param {Number} number Number which should be splited on smaller parts
 *
 * @returns {Array<Number} returns spited number.
 *
 * Example:
 * getChain(17) => [10, 7]
 * getChain(639) => [600, 30, 9]
 */
export default function getChain (number) {
  const steps = [1000, 100, 10]
  let chain = []
  // cache tail number which will be rewritten
  let n = number

  for (let i = 0; i <= 2; ++i) {
    // if first bigger then 1999
    if (i === 0) {
      const howManyTousands = Math.floor(n / 1000)
      // we need to use 1000 so many times
      if (howManyTousands > 1) {
        for (let h = 0; h < howManyTousands - 1; ++h) {
          chain.push(1000)
          n = n - 1000
        }
      }
    }

    const r = getSeparately(n, steps[i])

    // if last
    if (i === 2) {
      // add leftover
      chain = chain.concat(r)
      // and stop
      break
    }
    // if r = [main,tail]
    if (r.length > 1) {
      // push main
      chain.push(r[0])
      // cache the tail
      n = r[1]
    }
  }

  return chain
}
