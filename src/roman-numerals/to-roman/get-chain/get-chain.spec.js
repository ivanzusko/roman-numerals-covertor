import getChain from './index'

test('getChain', () => {
  expect(getChain(17)).toEqual([10, 7])
  expect(getChain(184)).toEqual([100, 80, 4])
  expect(getChain(901)).toEqual([900, 1])
})
