/**
 * @name getSeparately
 * @description takes number and devider, checkes if
 *            'number' is devidable by 'dev'. Then either return splited number,
 *            or the one chunk number (if not devidable)
 * 
 * @param {Number} num number which has to be splited
 * @param {Number} dev number which the `num` should be devidable on
 *              Can be [1000, 100, 10]
 *
 * @returns {Array<Number>}
 * 
 * Example:
 * getSeparately(150, 100) => [100, 50]
 * getSeparately(1890, 1000) => [1000, 890]
 */
export default function getSeparately (num, dev) {
  const yes = num % dev

  if (yes && (num > dev)) {
    return [num - yes, yes]
  }

  return [num]
}
