import getSeparately from './index'

test('getSeparately', () => {
  expect(getSeparately(150, 100)).toEqual([100, 50])
  expect(getSeparately(10, 10)).toEqual([10])
  expect(getSeparately(50, 10)).toEqual([50])
  expect(getSeparately(1890, 10)).toEqual([1890])
  expect(getSeparately(1890, 100)).toEqual([1800, 90])
  expect(getSeparately(1890, 1000)).toEqual([1000, 890])
})
