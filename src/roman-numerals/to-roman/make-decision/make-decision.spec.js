import makeDecision from './index'

test('makeDecision', () => {
  expect(makeDecision(100)).toEqual('C')
  expect(makeDecision(70)).toEqual('LXX')
  expect(makeDecision(8)).toEqual('VIII')
})
