import getLetters from '../get-letters'

/**
 * @name makeDecision
 * @description this function decides the scenario for the number conversion.
 *          Originaly the target number will be splited on numbers devidable by 10(and the last `tail`, tail < 10)
 *          and then one by one will be passed in here
 * @param {Number} n number which has to be converted to Roman equivalent
 *
 * @returns {String} the roman equivalent of passed number
 *
 * Example:
 * The target number to convert is 178.
 * Then 178 will be splited on [100, 70, 8].
 * Then one by one they will be passed here:
 *
 * makeDecision(100) => C
 * makeDecision(70) => LXX
 * makeDecision(8) => VIII
 *
 * Later they will be concatenated to CLXXVIII
 */
export default function makeDecision (n) {
  if (n <= 10) {
    return getLetters(n, 10, ['I', 'V', 'X'])
  } else if (n <= 100) {
    return getLetters(n, 100, ['X', 'L', 'C'])
  } else {
    return getLetters(n, 1000, ['C', 'D', 'M'])
  }
}
