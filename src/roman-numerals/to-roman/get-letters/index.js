import { invalidNumeral } from '../../utils/errors'
/**
 * Main Roman numerals:
 * M - 1000
 * D - 500
 * C - 100
 * L - 50
 * X - 10
 * V - 5
 * I - 1
 *
 * main ranges:
 * 1, 5, 10
 * 10, 50, 100
 * 100, 500, 1000
 */
/**
 * @name getLetters
 * @description this function takes number and returns the Roman equivalent
 * @param {Number} n number which should get it's roman value equivalent
 *                It will be devidable by 10 or less then 10.
 * @param {Number} max maximum range where the the number can be used
 * @param {Array<String>} letters Array of roman numerals which can be used withing the range
 *
 * @returns {String} the Roman numeral equivalent of passed number
 *
 * Example:
 * getLetters(8, 10, ['I', 'V', 'X']) => 'VIII'
 */
export default function getLetters (n, max, letters) {
  const step = max / 10

  if (n < step || n > max) {
    invalidNumeral()
  }

  // Step 1. Almost Emperor
  if (n >= max) {
    if (n === max) {
      return letters[2]
    }
    // more then 10 => another league
  } else if (n >= (5 * step)) {
    // Step 2. Between Patricians and Plebs

    if (n === (5 * step)) {
      return letters[1]
    }

    if ((step - max) === (n * -1)) {
      // we need return I for 1 and X for 10
      return letters[0] + letters[2] // IX
    }

    const times = n / step - 5
    let tail = ''
    let i = 0

    while (i < times) {
      tail = tail + letters[0]
      ++i
    }

    return letters[1] + tail
  } else {
    // Step 3. Between Plebs and Gladiators

    if ((step - 5 * step) === (n * -1)) {
      return letters[0] + letters[1]
    }

    const times = n / step
    let tail = ''
    let i = 0

    while (i < times) {
      tail = tail + letters[0]
      ++i
    }

    return tail
  }
}
