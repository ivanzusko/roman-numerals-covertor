import getLetters from './index'

test('getLetters', () => {
  expect(getLetters(3, 10, ['I', 'V', 'X'])).toEqual('III')
  expect(getLetters(4, 10, ['I', 'V', 'X'])).toEqual('IV')
  expect(getLetters(5, 10, ['I', 'V', 'X'])).toEqual('V')
  expect(getLetters(6, 10, ['I', 'V', 'X'])).toEqual('VI')
  expect(getLetters(7, 10, ['I', 'V', 'X'])).toEqual('VII')
  expect(getLetters(8, 10, ['I', 'V', 'X'])).toEqual('VIII')
  expect(getLetters(30, 100, ['X', 'L', 'C'])).toEqual('XXX')
  expect(getLetters(200, 1000, ['C', 'D', 'M'])).toEqual('CC')
  expect(getLetters(800, 1000, ['C', 'D', 'M'])).toEqual('DCCC')
})
