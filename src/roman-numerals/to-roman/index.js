import getChain from './get-chain'
import makeDecision from './make-decision'

/**
 * @name toRoman
 * @description method to convert regular nambers to Roman numerals
 * @param {String|Number} n value which has to be converted to Roman numerals
 *
 * @returns {String} the Roman numeral equivalent of passed value
 *
 * Example:
 * toRoman(7) => 'VII'
 * toRoman('7') => 'VII'
 * toRoman(1987) => 'MCMLXXXVII'
 */
export function toRoman (n) {
  return getChain(Number(n)).reduce((romanNumber, item) => {
    const roman = makeDecision(item)

    romanNumber = romanNumber.concat(roman)

    return romanNumber
  }, '')
}
