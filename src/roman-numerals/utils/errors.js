function invalidRomanNumeral () {
  throw new Error('Please, provide a valid Roman numeral')
}

function invalidNumeral () {
  throw new Error('Please, provide number bigger then 0... There was now concept of negatives or \'zero\' in those ancient times, when Roman Numerals were invented.')
}

export { invalidRomanNumeral, invalidNumeral }
